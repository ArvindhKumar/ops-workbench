var webpackConfig = require('./webpack.test.config.js');

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      'tests/test_index.js'
    ],
    preprocessors: {
      'tests/test_index.js': ['webpack']
    },
    webpack: webpackConfig,
    coverageIstanbulReporter: {
      reports: [ 'text-summary','html'],
      fixWebpackSourcePaths: true
    },
    reporters: ['spec','coverage-istanbul'],
    autoWatch: true,
    logLevel: config.LOG_ERROR,
    browsers: ['Chrome']
  })
}