const navigation = [{
  'type': 'item',
  'text': 'Nav1',
  'link': '/',
  'key': 1
}, {
  'type': 'item',
  'text': 'Nav2',
  'link': '/',
  'key': 2
}, {
  'type': 'dropdown',
  'text': 'Nav3',
  'key': 3,
  'items': [{
    'type': 'item',
    'text': 'Sub Nav1',
    'link': '/',
    'key': 3.1
  }, {
    'type': 'item',
    'text': 'Sub Nav2',
    'link': '/',
    'key': 3.2
  }]
}

]

export default navigation
