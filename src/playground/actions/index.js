import fetchJsonp from 'fetch-jsonp';
import http from 'axios';
import OrdersClient from '../../common/service-clients/orders'

const requestCompetitions = () => {
  return {
    type: 'REQ_COMP'
  }
}

const requestTeams = (competitionId) => {
  return {
    type: 'REQ_TEAM',
    competitionId
  }
}

const requestPlayers = (teamId) => {
  return {
    type: 'REQ_PLAYER',
    teamId
  }
}

const receiveCompetitions = (competitions) => {
  return {
    type: 'REC_COMP',
    competitions
  }
}

const receiveTeams = (competitionId, teams) => {
  return {
    type: 'REC_TEAM',
    competitionId,
    teams
  }
}

const receivePlayers = (teamId, players) => {
  return {
    type: 'REC_PLAYER',
    teamId,
    players
  }
}

export const fetchCompetitions = () => dispatch => {
  dispatch(requestCompetitions());

  return http.get('/ops-web/playground/api/competitions')
      .then(json => {
        dispatch(receiveCompetitions(json.data))
      });
}

export const fetchTeams = (compId) => dispatch => {
  dispatch(requestTeams());
  return http.get('/ops-web/playground/api/competitions/'+compId+'/teams')
      .then(json => {
        dispatch(receiveTeams(compId, json.data))
      });
}

export const fetchPlayers = (teamId) => dispatch => {
  dispatch(requestPlayers());
  return http.get('/ops-web/playground/api/teams/'+teamId+'/players')
      .then(json => {
        dispatch(receivePlayers(teamId, json.data))
      });
}
