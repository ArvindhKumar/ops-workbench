import { combineReducers } from 'redux'
import playground from './playground'

const playgroundApp = combineReducers({
  playground
})

export default playgroundApp