const playground = (state = [], action) => {
  switch (action.type) {
    case 'REQ_COMP':
      return Object.assign({}, state, {
        isFetching: true
      });
    case 'REC_COMP':
      return Object.assign({}, state, {
        isFetching: false,
        competitions: action.competitions
      });
    case 'REQ_TEAM':
      return Object.assign({}, state, {
        isFetching: true
      });
    case 'REC_TEAM':
      const updatedComps = state.competitions.map(competition => {
        if (competition.id + "" === action.competitionId) {
          return Object.assign({}, competition, action.teams);
        }
        return competition
      })

      return Object.assign({}, state, {
        isFetching: false,
        competitions: updatedComps
      });

    case 'REQ_PLAYER':
      return Object.assign({}, state, {
        isFetching: true
      });

    case 'REC_PLAYER':
      const modifiedComps = state.competitions.map(competition => {
        if (competition && competition.teams) {
          const updatedTeams = competition.teams.map(team => {
            if (team._links.self.href.split("/")
              .pop() === action.teamId) {
              return Object.assign({}, team, action.players);
            }
            return team
          })
          return Object.assign({}, competition, {
            teams: updatedTeams
          });
        }
        return competition
      })
      return Object.assign({}, state, {
        isFetching: false,
        competitions: modifiedComps
      });

    default:
      return state
  }
}

export default playground
