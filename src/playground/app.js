import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import playgroundApp from './reducers'
import thunkMiddleware from 'redux-thunk'
import { createLogger }  from 'redux-logger'
import App from './components/App'
import Promise from 'promise-polyfill'; 

// To add to window
if (!window.Promise) {
  window.Promise = Promise;
}

const loggerMiddleware = createLogger()

let store = createStore(playgroundApp, 
	applyMiddleware(
    	thunkMiddleware, 
    	loggerMiddleware 
  	)
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)