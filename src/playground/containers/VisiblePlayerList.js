import { connect } from 'react-redux'
import { fetchPlayers } from '../actions'
import PlayerList from '../components/PlayerList'
import _ from 'lodash';

const getPlayers = function(state, ownprops){
  let players = []
  if(state.playground && state.playground.competitions){ 
    state.playground.competitions.forEach(function(comp){
      if(comp.teams){
        var team = _.find(comp.teams, function(team) {
          if(team && team._links && team._links.team){
            return team._links.team.href.split("/").pop() ==  ownprops.match.params.teamId;
          }
        });
        if(team && team.players){
          players = team.players;
          return players;
        }
      }
    })
  }
  return players;
}
const mapStateToProps = (state, ownprops) => {
  return {
    players: getPlayers(state, ownprops),
    isFetching: state.playground.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchPlayers: (teamId) => {
      dispatch(fetchPlayers(teamId))
    }
  }
}

const VisiblePlayerList = connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayerList)

export default VisiblePlayerList