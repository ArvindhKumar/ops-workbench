import { connect } from 'react-redux'
import { fetchTeams } from '../actions'
import TeamList from '../components/TeamList'
import _ from 'lodash';

const getTeams = function(state, ownprops){
  if(state.playground && state.playground.competitions){
    var competition = _.find(state.playground.competitions, function(i) {
      return i.id ==  ownprops.match.params.compId;
    });
    if(competition && competition.teams){
      return competition.teams;
    }
  }
  return [];
}
const mapStateToProps = (state, ownprops) => {
  return {
    teams: getTeams(state, ownprops),
    isFetching: state.playground.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchTeams: (compId) => {
      dispatch(fetchTeams(compId))
    }
  }
}

const VisibleTeamList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TeamList)

export default VisibleTeamList