import { connect } from 'react-redux'
import { fetchCompetitions } from '../actions'
import CompetitionList from '../components/CompetitionList'

const mapStateToProps = (state) => {
  return {
    competitions: state.playground.competitions || [],
    isFetching: state.playground.isFetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchCompetitions: () => {
      dispatch(fetchCompetitions())
    }
  }
}

const VisibleCompetitionList = connect(
  mapStateToProps,
  mapDispatchToProps
)(CompetitionList)

export default VisibleCompetitionList