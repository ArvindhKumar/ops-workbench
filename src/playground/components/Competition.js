import React, { PropTypes } from 'react'
import {Route, Link } from 'react-router-dom'

const Competition = ({ id, caption, league, numberOfTeams, url }) => (
  <li>
  	<Link to={url}>{caption}</Link>
  </li>
)

Competition.propTypes = {
  id: PropTypes.number.isRequired,
  numberOfTeams: PropTypes.number.isRequired,
  caption: PropTypes.string.isRequired,
  league: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
}

export default Competition