import React, { PropTypes } from 'react'
import {Route, Link } from 'react-router-dom'

const Player = ({ name }) => (
  <li>
    {name}
  </li>
)

Player.propTypes = {
  name: PropTypes.string.isRequired
}

export default Player