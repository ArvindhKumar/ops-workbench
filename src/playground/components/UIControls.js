import React, { Component, PropTypes } from 'react'
import UIModal from '../../common/components/controls/modal'
import UIDropdown from '../../common/components/controls/dropdown'
import UIPagination from '../../common/components/controls/pagination'
import UITextBox from '../../common/components/controls/textbox'
import UIProgressBar from '../../common/components/controls/progressbar'
import UIBarChart from '../../common/components/charts/bar'
import UIDoughnutChart from '../../common/components/charts/doughnut'
import UIDataGrid from '../../common/components/controls/datagrid'
import { Button, Popover, OverlayTrigger, ButtonToolbar } from 'react-bootstrap';
import { Editors, Formatters} from 'react-data-grid-addons'
import faker from 'faker'

const AutoCompleteEditor = Editors.AutoComplete
const DropDownEditor = Editors.DropDownEditor
const ImageFormatter = Formatters.ImageFormatter

class UIControls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };
  }

  onModalClose() {
    this.setState({
      showModal: false
    });
  }

  onDropDownSelect(selectedItem){
    console.log(selectedItem);
  }

  onPageChange(pageNo){
    console.log("Select page no:"+pageNo);
  }

  createRows(numberOfRows) {
    let rows = [];
    for (let i = 0; i < numberOfRows; i++) {
      rows[i] = this.createFakeRowObjectData(i);
    }
    return rows;
  }

  createFakeRowObjectData(index) {
    return {
      id: 'id_' + index,
      index: index,
      avartar: faker.image.avatar(),
      county: faker.address.county(),
      email: faker.internet.email(),
      title: faker.name.prefix(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      street: faker.address.streetName(),
      zipCode: faker.address.zipCode(),
      date: faker.date.past().toLocaleDateString(),
      bs: faker.company.bs(),
      catchPhrase: faker.company.catchPhrase(),
      companyName: faker.company.companyName(),
      words: faker.lorem.words(),
      sentence: faker.lorem.sentence()
    };
  }

  render() {
    let dropdownOptions = [{'id': '1', 'value': 'Option 1'}, {'id': '2', 'value': 'Option 2'}, {'id': '3', 'value': 'Option 3'}];
    let progressBarItems = [{'id': '1', 'percent': 20, 'style': 'success'}, {'id': '2', 'percent': 50, 'style': 'warning'}]
    let barChartData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Sample bar chart data',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: [65, 59, 80, 81, 56, 55, 40]}
      ]
    };
    let doughnutChartData = {
      labels: [
        'Red',
        'Green',
        'Yellow'
      ],
      datasets: [{
        data: [300, 50, 100],
        backgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
        ],
        hoverBackgroundColor: [
          '#FF6384',
          '#36A2EB',
          '#FFCE56'
        ]
      }]
    };

    const counties = [
      { id: 0, title: 'Bedfordshire'},
      { id: 1, title: 'Berkshire'},
      { id: 2, title: 'Buckinghamshire'},
      { id: 3, title: 'Cambridgeshire'},
      { id: 4, title: 'Cheshire'},
      { id: 5, title: 'Cornwall'},
      { id: 6, title: 'Cumbria, (Cumberland)'},
      { id: 7, title: 'Derbyshire'},
      { id: 8, title: 'Devon'},
      { id: 9, title: 'Dorset'}
    ]

    const titles = ['Dr.', 'Mr.', 'Mrs.', 'Miss', 'Ms.'];

    const dataGridColumns = [
      {
        key: 'id',
        name: 'ID',
        width: 80,
        resizable: true
      },
      {
        key: 'avartar',
        name: 'Avartar',
        width: 60,
        formatter: ImageFormatter,
        resizable: true,
        headerRenderer: <ImageFormatter value={faker.image.cats()} />
      },
      {
        key: 'county',
        name: 'County',
        editor: <AutoCompleteEditor options={counties}/>,
        width: 200,
        resizable: true
      },
      {
        key: 'title',
        name: 'Title',
        editor: <DropDownEditor options={titles}/>,
        width: 200,
        resizable: true,
        events: {
          onDoubleClick: function() {
            console.log('The user double clicked on title column');
          }
        }
      },
      {
        key: 'firstName',
        name: 'First Name',
        editable: true,
        width: 200,
        resizable: true,
        sortable: true,
        filterable: true
      },
      {
        key: 'lastName',
        name: 'Last Name',
        editable: true,
        width: 200,
        resizable: true,
        sortable: true,
        filterable: true
      },
      {
        key: 'email',
        name: 'Email',
        editable: true,
        width: 200,
        resizable: true,
        sortable: true,
        filterable: true
      }
    ];

    const dataGridRows = this.createRows(100)


    const popoverBottom = (
      <Popover id="popover-positioned-scrolling-bottom" title="Popover">
        <strong>Sample popover</strong> Check this.
      </Popover>
    )

    return ( 
      <div>
        <ul>
          <li className="m20">
            <div className="title"> Modal Sample</div>
            <Button
              bsStyle="primary"
              bsSize="large"
              onClick={() => this.setState({ showModal: true})}
            >
            Open modal
            </Button>
            <UIModal show={this.state.showModal} onClose={this.onModalClose.bind(this)}/>
          </li>
          <li className="m20">
            <div className="title"> Drop down Samples </div>
            <UIDropdown style="default" title="Default" id="dd1" options={dropdownOptions} onSelect={this.onDropDownSelect.bind(this)}/>
            <UIDropdown style="primary" title="Primary" id="dd2" options={dropdownOptions} onSelect={this.onDropDownSelect.bind(this)}/>
          </li>
          <li className="m20">
            <div className="title">Popover Sample</div>
            <OverlayTrigger container={this} trigger="click" placement="right" overlay={popoverBottom}>
              <Button>Popover test!</Button>
            </OverlayTrigger>
          </li>
          <li className="m20">
            <div className="title">Pagination Sample</div>
            <UIPagination noOfPages={40} onPageChange={this.onPageChange.bind(this)}/>
          </li>
          <li className="m20">
            <div className="title"> Text box Sample </div>
            <UITextBox placeholder="Enter the text" helpText="Name should be between 3 and 20 chars" title="Please enter your name" minLength={3} maxLength={20} />
          </li>
          <li className="m20">
            <div className="title">Sample Progress Bar</div>
            <UIProgressBar items={progressBarItems}/>
          </li>
          <li className="m20">
            <div className="title">Sample Bar Chart</div>
            <UIBarChart data={barChartData} height={400} width={50}/>
          </li>
          <li className="m20">
            <div className="title">Sample Doughnut Chart</div>
            <UIDoughnutChart data={doughnutChartData}/>
          </li>
          <li className="m20">
            <div className="title">Sample Editable Data Grid</div>
            <UIDataGrid rows={dataGridRows} columns={dataGridColumns} createNewRow={this.createFakeRowObjectData.bind(this)}/>
          </li>
        </ul>
    </div>
  )
  }
}

export default UIControls