import React, { PropTypes } from 'react'
import {Route, Link } from 'react-router-dom'

const crestStyle = {
  height: '30px',
  width: '30px'
};
const Team = ({ code, name, crestUrl,  url }) => (
  <li>
  	<img src={crestUrl} style={crestStyle}/>
    <Link to={url}>{name}</Link>
  </li>
)

Team.propTypes = {
  code: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  crestUrl: PropTypes.string.isRequired
}

export default Team