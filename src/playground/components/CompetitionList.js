import React, { Component, PropTypes } from 'react'
import Competition from './Competition'
import { fetchCompetitions } from '../actions'

class CompetitionList extends Component {

  componentDidMount() {
    const { dispatch } = this.props
    this.props.onFetchCompetitions();
  }
  render() {
    const { isFetching } = this.props
    let elem = null;

    if(!isFetching){
      elem =  <ul>
      {this.props.competitions.map(competition =>
        <Competition
        key={competition.id}
        url={"/ops-web/playground/"+competition.id+"/teams"}
        {...competition}
        />
      )}
      </ul>
    }
    if(isFetching){
      elem = <h2>Loading Competitions...</h2>
    }
    return ( 
      <div>
    {elem}
    </div>
  )
  }
}

export default CompetitionList