import React, { Component, PropTypes } from 'react'
import Team from './Team'

class TeamList extends Component {

  componentDidMount() {
    const { dispatch } = this.props
    this.props.onFetchTeams(this.props.match.params.compId);
  }
  render() {
    const { isFetching } = this.props
    let elem = null
    if(!isFetching){
      elem =  <ul>
      {this.props.teams.map(team =>
        <Team
        key={team.code}
        url={"/ops-web/playground/teams/"+team._links.self.href.split("/").pop()+"/players"}
        {...team}
        />
      )}
      </ul>
    }
    if(isFetching){
      elem = <h2>Loading Teams...</h2>
    }
    return ( 
      <div>
    {elem}
    </div>
  )
  }
}

export default TeamList