import React, { Component, PropTypes } from 'react'
import Player from './Player'

class PlayerList extends Component {

  componentDidMount() {
    const { dispatch } = this.props
    this.props.onFetchPlayers(this.props.match.params.teamId);
  }
  render() {
    const { isFetching } = this.props
    let elem = null
    if(!isFetching){
      elem =  <ul>
      {this.props.players.map(player =>
        <Player
        key={player.jerseyNumber}
        {...player}
        />
      )}
      </ul>
    }
    if(isFetching){
      elem = <h2>Loading Players...</h2>
    }
    return ( 
      <div>
    {elem}
    </div>
  )
  }
}

export default PlayerList