import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Header from '../../common/components/header'
import Footer from '../../common/components/footer'
import SideNav from '../../common/components/sidenav'

import VisibleCompetitionList from '../containers/VisibleCompetitionList'
import VisibleTeamList from '../containers/VisibleTeamList'
import VisiblePlayerList from '../containers/VisiblePlayerList'

import UIControls from './UIControls'
import navigation from '../constants/Navigation'

const HeaderRender = (props) => {
  return (
    <Header 
      navItems={navigation}
    />
  );
}

const routes = [
  { path: '/ops-web/playground',
  	exact: true,
    header: HeaderRender,
    footer: Footer,
    sidebar: SideNav,
    main: VisibleCompetitionList
  },
  { path: '/ops-web/playground/:compId/teams',
    exact: true,
    header: HeaderRender,
    footer: Footer,
    sidebar: SideNav,
    main: VisibleTeamList
  },
  { path: '/ops-web/playground/teams/:teamId/players',
    exact: true,
    header: HeaderRender,
    footer: Footer,
    sidebar: SideNav,
    main: VisiblePlayerList
  },
  { path: '/ops-web/playground/ui',
    exact: true,
    header: HeaderRender,
    footer: Footer,
    sidebar: SideNav,
    main: UIControls
  }
]

const App = () => (
  <Router>
  <div>
    {routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            render={route.header}
          />
        ))}

    <div className="main">
    	{routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        ))}
    </div>

     <div className="footer">
     	    {routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.footer}
          />
        ))}
     </div>
  </div>
  </Router>
)
export default App