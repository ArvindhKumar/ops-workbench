import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import pricingApp from './reducers'
import thunkMiddleware from 'redux-thunk'
import { createLogger }  from 'redux-logger'
import App from './components/App'

const loggerMiddleware = createLogger()

let store = createStore(pricingApp, 
	applyMiddleware(
    	thunkMiddleware, 
    	loggerMiddleware 
  	)
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)