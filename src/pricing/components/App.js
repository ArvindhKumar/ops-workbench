import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import Header from '../../common/components/header'
import Footer from '../../common/components/footer'
import SideNav from '../../common/components/sidenav'
import navigation from '../constants/Navigation'

const HeaderRender = (props) => {
  return (
    <Header 
      navItems={navigation}
    />
  );
}

const routes = [
  { path: '/ops-web/pricing',
  	exact: true,
    header: HeaderRender,
    footer: Footer,
    sidebar: SideNav,
    main: () => <h2><Link to="/ops-web/pricing/products">Products</Link></h2>
  },
  { path: '/ops-web/pricing/products',
    header: HeaderRender,
    footer: Footer,
    sidebar: SideNav,
    main: () => <h2><Link to="/ops-web/pricing">Pricing Home</Link></h2>
  }
]

const App = () => (
  <Router>
  <div>
    {routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            render={route.header}
          />
        ))}

    <div className="main">
    	{routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        ))}
    </div>

     <div className="footer">
     	    {routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.footer}
          />
        ))}
     </div>
  </div>
  </Router>
)
export default App