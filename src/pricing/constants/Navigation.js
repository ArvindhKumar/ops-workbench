const navigation = [{
  'type': 'item',
  'text': 'Priving Nav1',
  'link': '/',
  'key': 1
}, {
  'type': 'item',
  'text': 'Pricing Nav2',
  'link': '/',
  'key': 2
}, {
  'type': 'dropdown',
  'text': 'Pricing Nav3',
  'key': 3,
  'items': [{
    'type': 'item',
    'text': 'Pricing Sub Nav1',
    'link': '/',
    'key': 3.1
  }, {
    'type': 'item',
    'text': 'Pricing Sub Nav2',
    'link': '/',
    'key': 3.2
  }]
}

]

export default navigation
