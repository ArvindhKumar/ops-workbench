import { combineReducers } from 'redux'
import searchFilter from './searchFilter'

const pricingApp = combineReducers({
  searchFilter
})

export default pricingApp