import { combineReducers } from 'redux'
import searchFilter from './searchFilter'

const csApp = combineReducers({
  searchFilter
})

export default csApp