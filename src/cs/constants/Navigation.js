const navigation = [{
  'type': 'item',
  'text': 'CS Nav1',
  'link': '/',
  'key': 1
}, {
  'type': 'item',
  'text': 'CS Nav2',
  'link': '/',
  'key': 2
}, {
  'type': 'dropdown',
  'text': 'CS Nav3',
  'key': 3,
  'items': [{
    'type': 'item',
    'text': 'CS Sub Nav1',
    'link': '/',
    'key': 3.1
  }, {
    'type': 'item',
    'text': 'CS Sub Nav2',
    'link': '/',
    'key': 3.2
  }]
}

]

export default navigation