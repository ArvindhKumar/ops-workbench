import http from 'axios';
import _ from 'lodash';

class BaseClient {
  constructor(options) {
    this.baseOptions = options || {};
    this.baseUrl = this.baseOptions.baseUrl || API_URL_BASE;
  }

  _get(options) {
    let me = this;
    let o = _.extend({}, me.baseOptions, options)
    return new Promise(function(resolve, reject) {
      http({
        url: o.url,
        method: 'get',
        baseURL: me.baseUrl,
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        params: o.params,
        responseType: 'json'
      })
      .then(json => {
        resolve(json.data);
      })
      .catch(response => {
        reject(response);
      })
    })
  }

  _post(options) {
    let me = this;
    let o = _.extend({}, me.baseOptions, options)
    return new Promise(function(resolve, reject) {
      http({
        url: o.url,
        method: 'post',
        baseURL: me.baseUrl,
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        params: o.params,
        data: o.data,
        responseType: 'json'
      })
      .then(json => {
        resolve(json.data);
      })
      .catch(response => {
        reject(response);
      })
    })
  }

  _put(options) {
    let me = this;
    let o = _.extend({}, me.baseOptions, options)
    return new Promise(function(resolve, reject) {
      http({
        url: o.url,
        method: 'put',
        baseURL: me.baseUrl,
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        params: o.params,
        data: o.data,
        responseType: 'json'
      })
      .then(json => {
        resolve(json.data);
      })
      .catch(response => {
        reject(response);
      })
    })
  }


  _del(options) {
    let me = this;
    let o = _.extend({}, me.baseOptions, options)
    return new Promise(function(resolve, reject) {
      http({
        url: o.url,
        method: 'delete',
        baseURL: me.baseUrl,
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        responseType: 'json'
      })
      .then(json => {
        resolve(json.data);
      })
      .catch(response => {
        reject(response);
      })
    })
  }

}


export default BaseClient