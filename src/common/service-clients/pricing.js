import BaseClient from './base'

class PricingClient extends BaseClient{

  constructor() {
    super();
    this.pricingV3Base = "/v3/products/";
  }

  getProductByProductId(productId){
    let url = this.pricingV3Base + productId;
    let options = {
      url: url
    }
    return this._get(options);
  }
}

export default PricingClient