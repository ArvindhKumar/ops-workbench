import BaseClient from './base'

class OrdersClient extends BaseClient{

  constructor() {
    super();
    this.orderv3Base = "/v3/orders/";
  }

  getOrderByOrderId(orderId){
    let url = this.orderv3Base + orderId;
    let options = {
      url: url
    }
    return this._get(options);
  }
}

export default OrdersClient