import React, { Component, PropTypes } from 'react'
import ReactDom from 'react-dom'
import { Pagination } from 'react-bootstrap';


class UIPagination extends Component {
  constructor(props){
    super(props);
    this.state = {
      activePage: props.activePage || 1
    };
  }

  handleSelect(eventKey) {
    this.setState({
      activePage: eventKey
    });

    if(this.props.onPageChange){
      this.props.onPageChange(eventKey);
    }
  }

  render() {
    return (
      <Pagination
        prev
        next
        first
        last
        ellipsis
        boundaryLinks
        items={this.props.noOfPages}
        maxButtons={5}
        activePage={this.state.activePage}
        onSelect={this.handleSelect.bind(this)} />
    );
  }
};

export default UIPagination