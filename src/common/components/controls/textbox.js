import React, { Component, PropTypes } from 'react'
import ReactDom from 'react-dom'
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';


class UITextBox extends Component {
  constructor(props){
    super(props);
    this.state = {
      value: props.value || '',
      hasEntered: false
    };
  }

  getValidationState() {
    if(!this.state.hasEntered){
      return null;
    }

    const length = this.state.value.length;
    let valState = 'success'
    if(this.props.minLength && length < this.props.minLength){
      valState = 'error'
    }
    if(this.props.maxLength && length > this.props.maxLength){
      valState = 'error'
    }
    return valState;
  }

  handleChange(event) {
    this.setState({ value: event.target.value, hasEntered: true });
  }

  render() {
    return (
     <FormGroup
          controlId="formBasicText"
          bsClass="textBox"
          validationState={this.getValidationState()}
        >
        <ControlLabel>{this.props.title}</ControlLabel>
        <FormControl
            type="text"
            value={this.state.value}
            placeholder={this.props.placeholder}
            onChange={this.handleChange.bind(this)}
          />
        <FormControl.Feedback />
        <HelpBlock>{this.props.helpText}</HelpBlock>
      </FormGroup>
    );
  }
};

export default UITextBox