import React, { Component, PropTypes } from 'react'
import ReactDom from 'react-dom'
import { DropdownButton, MenuItem } from 'react-bootstrap';
import _ from 'lodash';

class UIDropdown extends Component{
  constructor(props){
    super(props);
    this.state = {
      value: props.title || "Select"
    };
  }

  getInitialState(){
    return {value: this.props.title || "Select"}
  }

  onSelect (key, event) {
    let selectedItem = _.find(this.props.options, function(i){
      return i.id == key;
    })

    this.setState({
      value: selectedItem.value
    })

    if(this.props.onSelect){
      this.props.onSelect(selectedItem);
    }
  }

  render() {
    return (
      <DropdownButton bsStyle={this.props.style} title={this.state.value} id={this.props.id} onSelect={this.onSelect.bind(this)}>
        {this.props.options.map(option =>
          <MenuItem
          key = {option.id}
          eventKey={option.id}
          >
          {option.value}
          </MenuItem>
        )}
      </DropdownButton>
    );
  }
};

export default UIDropdown