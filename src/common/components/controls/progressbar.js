import React, { Component, PropTypes } from 'react'
import ReactDom from 'react-dom'
import { ProgressBar } from 'react-bootstrap';


class UIProgressBar extends Component {
  constructor(props){
    super(props);
  }


  render() {
    return (
      <ProgressBar>
      {
        this.props.items.map(item =>
          <ProgressBar striped bsStyle={item.style} now={item.percent} key={item.id} label={`${item.percent}%`}/>
        )
      }
      </ProgressBar>
    );
  }
};

export default UIProgressBar