import React, { Component, PropTypes } from 'react'
import ReactDom from 'react-dom'
import { DropdownButton, MenuItem } from 'react-bootstrap';
import _ from 'lodash';
import faker from 'faker'
import ReactDataGrid from 'react-data-grid'
import { Toolbar, Data } from 'react-data-grid-addons'

const Selectors = Data.Selectors

class UIDataGrid extends Component{
  constructor(props){
    super(props);
    this.state = {
      rows: this.props.rows || [],
      filters: {},
      sortColumn: null,
      sortDirection: null
    };
  }

  getColumns() {
    let clonedColumns = this.props.columns.slice();
    clonedColumns[2].events = {
      onClick: (ev, args) => {
        const idx = args.idx;
        const rowIdx = args.rowIdx;
        this.grid.openCellEditor(rowIdx, idx);
      }
    };

    return clonedColumns;
  }

  handleGridRowsUpdated({ fromRow, toRow, updated }) {
    let rows = this.state.rows.slice();

    for (let i = fromRow; i <= toRow; i++) {
      let rowToUpdate = rows[i];
      let updatedRow = _.extend({}, rowToUpdate, updated);
      rows[i] = updatedRow;
    }

    this.setState({ rows });
  }

  handleAddRow({ newRowIndex }) {
    const newRow = this.props.createNewRow(newRowIndex);

    let rows = this.state.rows.slice();
    rows.push(newRow)
    this.setState({ rows });
  }

  getRowAt(index) {
    if (index < 0 || index > this.getSize()) {
      return undefined;
    }
    const rows = this.getRows();
    return rows[index];
  }

  getSize() {
    return this.getRows().length;
  }

  getRows() {
    return Selectors.getRows(this.state);
  }

  handleGridSort(sortColumn, sortDirection) {
    this.setState({ sortColumn: sortColumn, sortDirection: sortDirection });
  }

  handleFilterChange(filter) {
    let newFilters = Object.assign({}, this.state.filters);
    if (filter.filterTerm) {
      newFilters[filter.column.key] = filter;
    } else {
      delete newFilters[filter.column.key];
    }

    this.setState({ filters: newFilters });
  }

  onClearFilters() {
    this.setState({ filters: {} });
  }

  render() {
    return (
      <ReactDataGrid
        ref={ node => this.grid = node }
        enableCellSelect={true}
        onGridSort={this.handleGridSort.bind(this)}
        onAddFilter={this.handleFilterChange.bind(this)}
        onClearFilters={this.onClearFilters.bind(this)}
        columns={this.getColumns()}
        rowGetter={this.getRowAt.bind(this)}
        rowsCount={this.getSize()}
        onGridRowsUpdated={this.handleGridRowsUpdated.bind(this)}
        toolbar={<Toolbar onAddRow={this.handleAddRow.bind(this)} enableFilter={true}/>}
        enableRowSelect={true}
        rowHeight={50}
        minHeight={600}
        rowScrollTimeout={200} />
    );
  }
};

export default UIDataGrid