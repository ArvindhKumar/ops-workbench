import React, { Component, PropTypes } from 'react'
import ReactDom from 'react-dom'
import { Modal, Button } from 'react-bootstrap';


class UIModal extends Component {
  render() {
    let close = () => this.props.onClose();

    return (
        <Modal
          show={this.props.show}
          onHide={close}
          container={this}
          aria-labelledby="contained-modal-title"
          bsSize="large"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title">Contained Modal</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            This is a sample modal.  This is a sample modal.  This is a sample modal.
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={close}>Close</Button>
          </Modal.Footer>
        </Modal>
    );
  }
};

export default UIModal