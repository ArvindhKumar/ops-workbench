import React from 'react'
import ReactDom from 'react-dom'
import { Navbar, NavItem, Nav, NavDropdown, MenuItem } from 'react-bootstrap';

const Header = (props) => {
  return ( < Navbar inverse collapseOnSelect >
    < Navbar.Header >
    < Navbar.Brand >
    < a href = "#" > OPS - Workbench - Tools < /a> < /Navbar.Brand > < Navbar.Toggle / >
    < /Navbar.Header> < Navbar.Collapse > < Nav > {
      props.navItems.map(item => {
        if (item.type == 'item') {
          return <NavItem key = { item.key }
          eventKey = { item.key }
          href = "#" > { item.text } < /NavItem>
        } else if (item.type == 'dropdown') {
          return <NavDropdown key = { item.key }
          title = { item.text }
          id = "basic-nav-dropdown"
          eventKey = { item.key } > {
            item.items.map(subItem => {
              if (subItem.type == 'item') {
                return <MenuItem key = { subItem.key }
                eventKey = { subItem.key }
                href = "#" > { subItem.text } < /MenuItem>
              }
            })
          } < /NavDropdown>
        }
      })
    }
  } < /Nav> < /Navbar.Collapse > < /Navbar>
)
};

export default Header
