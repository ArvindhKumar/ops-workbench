import React, { Component, PropTypes } from 'react'
import {Doughnut} from 'react-chartjs-2';


class UIDoughnutChart extends Component {
  render() {
    return (
      <div>
        <Doughnut data={this.props.data} />
      </div>
    );
  }
};

export default UIDoughnutChart

