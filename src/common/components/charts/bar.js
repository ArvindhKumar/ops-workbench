import React, { Component, PropTypes } from 'react'
import {Bar} from 'react-chartjs-2';


class UIBarChart extends Component {
  constructor(props){
    super(props);
    this.state = {
      activePage: props.activePage || 1
    };
  }

  handleSelect(eventKey) {
    this.setState({
      activePage: eventKey
    });

    if(this.props.onPageChange){
      this.props.onPageChange(eventKey);
    }
  }

  render() {
    return (
      <div>
        <Bar
          data={this.props.data}
          width={this.props.width}
          height={this.props.height}
          options={{
            maintainAspectRatio: false
          }}
        />
      </div>
    );
  }
};

export default UIBarChart
