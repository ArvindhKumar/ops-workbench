'use strict'
/**
 *
 *  This is the module where the hapi server is started.
 *
 *
 **/
module.exports = function() {
  let hapi = require('hapi'),
    RegisterPublicRoutes = require('./lib/routes/register-routes'),
    registerPublicRoutes,
    HapiConfig = require('hapi-config'),
    WebpackPlugin =  require('hapi-webpack-plugin'),
    path = require('path'),
    _ = require('lodash'),
    dependencies = {},
    server,
    env,
    config;

  server = new hapi.Server();

  server.register({
        register: HapiConfig
    }
  );

  env = process.env.NODE_ENV === 'production' ? 'prod' : 'dev';
  config = server.plugins['hapi-config'].loadConfig('./configs/hapi.' + env + ".json");

  server = new hapi.Server();

  if(env == 'dev'){
    server.register({
        register: WebpackPlugin,
        options: './webpack.dev.config.js'
        }
    );
  }

  let publicConnection = server.connection({
    port: config.get('server:port'),
    routes: {
      cors: true
    },
    host: config.get('server:host')
  });


  server.register(require('inert'), (err) => {
    if (err) {
        throw err;
    }
    try{
      registerPublicRoutes = new RegisterPublicRoutes(dependencies, config);
      registerPublicRoutes.registerRoutes(publicConnection);
      server.start(function() {
        _.forEach(server.connections, function(connection) {
          console.log('Server started at: ' + connection.info.uri);
        });
      });
    }
    catch (err) {
      console.log('error', err);
      process.exit();
    }    
  });


}
