'use strict';

const RouteHandler = require('./route-handler'),
  Wreck = require('wreck');

class RegisterPublicRoutes {
  constructor() {
    this.routeHandler = new RouteHandler();
  }

  registerRoutes(server) {
    const me = this;
    server.log('Registering public routes for Blueprint service');

    server.route({
      method: 'GET',
      path: '/ops-web/pricing/{param*}',
      config: {
        handler: (request, reply) => me.routeHandler.getPricingHome(request, reply),
        description: 'Pricing Home',
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/ops-web/cs/{param*}',
      config: {
        handler: (request, reply) => me.routeHandler.getCSHome(request, reply),
        description: 'CS Home',
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/ops-web/playground/{param*}',
      config: {
        handler: (request, reply) => me.routeHandler.getPlaygroundHome(request, reply),
        description: 'Playground Home',
        state: {
          parse: false,
          failAction: 'log'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/ops-web/public/{param*}',
      handler: {
        directory: {
          path: './public'
        }
      }
    });

    server.route({
      method: 'GET',
      path: '/ops-web/playground/api/{ext*}',
      handler: function(request, reply) {
        Wreck.get('http://api.football-data.org/v1/' + request.params.ext, { headers: { 'X-Auth-Token': '14355965ce0243518c344f5606841203' } }, (err, res, payload) => {
          reply(payload);
        });
      }
    });
  }
}

module.exports = RegisterPublicRoutes;
