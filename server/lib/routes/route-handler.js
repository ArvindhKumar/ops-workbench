'use strict'

class RouteHandler {
  constructor() {
  }

  getBluePrintResponse(request, reply) {
    let me = this;
    reply('sample response')
  }

  getPricingHome(request, reply) {
    reply.file('./src/pricing/index.html');
  }

  getCSHome(request, reply) {
    reply.file('./src/cs/index.html');
  }

  getPlaygroundHome(request, reply) {
    reply.file('./src/playground/index.html');
  }
}

module.exports = RouteHandler;
