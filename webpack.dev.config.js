var path = require('path');
var WriteFilePlugin = require('write-file-webpack-plugin');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = {
   	entry: {
        pricing:  ['./src/pricing/app.js','./src/pricing/scss/main.scss'], 
        cs:  ['./src/cs/app.js','./src/cs/scss/main.scss'],
        playground:  ['./src/playground/app.js', './src/playground/scss/main.scss']
    },
  	output: {
    	filename: '[name].bundle.js',
    	path: path.join(__dirname, 'public')
  	},
    plugins: [
        new WriteFilePlugin(),
        new webpack.DefinePlugin({
          API_URL_BASE:JSON.stringify("http://www.testurl.com/")

      }),
        new ExtractTextPlugin({
          filename: '[name].bundle.css',
          allChunks: true,
        })
    ],
  	module: {
    	rules: [
    		{ test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
        { test: /\.scss$/, exclude: /node_modules/, loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader']) }
  		]
	}
};

