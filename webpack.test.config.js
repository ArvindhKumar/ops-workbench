var path = require('path');

module.exports = {
    entry: {
        pricing:  './src/pricing/app.js',
        cs:  './src/cs/app.js',
        playground:  './src/playground/app.js'
    },
    output: {
      filename: '[name].bundle.js',
      path: path.join(__dirname, 'dist')
    },
    plugins: [
    ],
    module: {
      rules: [
        { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' },
        { test: /\.js$/, exclude: /node_modules/, loader: 'istanbul-instrumenter-loader' ,options: {esModules: true}}
      ]
  }
};