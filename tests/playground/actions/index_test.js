import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { fetchCompetitions, fetchTeams, fetchPlayers } from '../../../src/playground/actions'
import MockAdapter from 'axios-mock-adapter'
import axios from 'axios'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('async actions', () => {

  it('creates REC_COMP when fetching competitions has been done', () => {
    const competitions = [{id: 1},{id: 2}]
    const mockAdapter = new MockAdapter(axios);
    mockAdapter.onGet('/playground/api/competitions').reply(200, competitions);


    const expectedActions = [
      { type: 'REQ_COMP' },
      { type: 'REC_COMP', competitions }
    ]
    const store = mockStore({  })

    return store.dispatch(fetchCompetitions())
      .then(() => { // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
        mockAdapter.restore();
      })
  })


  it('creates REC_TEM when fetching teams has been done', () => {
    const teams = [{id: 1},{id: 2}]
    const mockAdapter = new MockAdapter(axios);
    mockAdapter.onGet('/playground/api/competitions/1/teams').reply(200, teams);


    const expectedActions = [
      { type: 'REQ_TEAM', competitionId: undefined },
      { type: 'REC_TEAM', competitionId:1, teams: teams }
    ]
    const store = mockStore({  })

    return store.dispatch(fetchTeams(1))
      .then(() => { // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
        mockAdapter.restore();
      })

  })

  it('creates REC_PLAYERS when fetching players has been done', () => {
    const players = [{id: 1},{id: 2}]
    const mockAdapter = new MockAdapter(axios);
    mockAdapter.onGet('/playground/api/teams/1/players').reply(200, players);


    const expectedActions = [
      { type: 'REQ_PLAYER', teamId: undefined },
      { type: 'REC_PLAYER', teamId:1, players: players }
    ]
    const store = mockStore({  })

    return store.dispatch(fetchPlayers(1))
      .then(() => { // return of async actions
        expect(store.getActions()).toEqual(expectedActions)
        mockAdapter.restore();
      })
  })
})