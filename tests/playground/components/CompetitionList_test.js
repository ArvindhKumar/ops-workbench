import { renderIntoDocument } from 'react-addons-test-utils';
import React from 'react'
import { findDOMNode } from 'react-dom'
import { shallow } from 'enzyme';
import CompetitionList from '../../../src/playground/components/CompetitionList'
import createRouterContext from 'react-router-test-context'


describe('CompetitionList Component', () => {
    it ('should render the loading when isFetching is true', () => {
    	let context = createRouterContext();
    	let props = {isFetching: true, competitions: []}
        let component = shallow(<CompetitionList {...props}/>, {context: context});
        
        expect(component.node.type).toEqual('div');
        expect(component.node.props.children.props.children).toEqual('Loading Competitions...');
    });

    it ('should render competitions when isFetching is false', () => {
    	let context = createRouterContext();
    	let competitions = [{id:1, caption:"comp1", numberOfTeams:10}, {id:2, caption:"comp2", numberOfTeams:20}]
    	let props = {isFetching: false, competitions: competitions}
        let component = shallow(<CompetitionList {...props}/>, {context: context});

        expect(component.node.type).toEqual('div');
        expect(component.node.props.children.type).toEqual('ul');
        expect(component.node.props.children.props.children.length).toEqual(2);

        expect(component.node.props.children.props.children[0].type.name).toEqual('Competition');
        expect(component.node.props.children.props.children[0].props.caption).toEqual('comp1');
        expect(component.node.props.children.props.children[0].props.url).toEqual('/playground/1/teams');
        expect(component.node.props.children.props.children[0].props.numberOfTeams).toEqual(10);

        expect(component.node.props.children.props.children[1].type.name).toEqual('Competition');
        expect(component.node.props.children.props.children[1].props.caption).toEqual('comp2');
        expect(component.node.props.children.props.children[1].props.url).toEqual('/playground/2/teams');
        expect(component.node.props.children.props.children[1].props.numberOfTeams).toEqual(20);
    });

});