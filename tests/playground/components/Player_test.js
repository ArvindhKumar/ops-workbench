import { renderIntoDocument } from 'react-addons-test-utils';
import React from 'react'
import { findDOMNode } from 'react-dom'
import { shallow } from 'enzyme';
import Player from '../../../src/playground/components/Player'
import createRouterContext from 'react-router-test-context'


describe('Player Component', () => {
    it ('should render the component with caption and url', () => {
        let context = createRouterContext()
        let component = shallow(<Player id="1" name="test_player"/>, {context: context});
        expect(component.node.type).toEqual('li');
        expect(component.node.props.children).toEqual('test_player');
    });
});