import { renderIntoDocument } from 'react-addons-test-utils';
import React from 'react'
import { findDOMNode } from 'react-dom'
import { shallow } from 'enzyme';
import Team from '../../../src/playground/components/Team'
import createRouterContext from 'react-router-test-context'


describe('Team Component', () => {
    it ('should render the component with caption and url', () => {
    	let context = createRouterContext()
        let component = shallow(<Team crestUrl="crest_url" name="team_name"  url="url"/>, {context: context});
        expect(component.node.type).toEqual('li');
        expect(component.node.props.children[0].type).toEqual('img');
        expect(component.node.props.children[1].type.name).toEqual('Link');
        expect(component.node.props.children[1].props.children).toEqual('team_name');
        expect(component.node.props.children[0].props.src).toEqual('crest_url');
        expect(component.node.props.children[1].props.to).toEqual('url');
    });
});