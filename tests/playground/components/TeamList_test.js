import { renderIntoDocument } from 'react-addons-test-utils';
import React from 'react'
import { findDOMNode } from 'react-dom'
import { shallow } from 'enzyme';
import TeamList from '../../../src/playground/components/TeamList'
import createRouterContext from 'react-router-test-context'


describe('TeamList Component', () => {
    it ('should render the loading when isFetching is true', () => {
    	let context = createRouterContext();
    	let props = {isFetching: true, teams: []}
        let component = shallow(<TeamList {...props}/>, {context: context});
        
        expect(component.node.type).toEqual('div');
        expect(component.node.props.children.props.children).toEqual('Loading Teams...');
    });

    it ('should render teams when isFetching is false', () => {
    	let context = createRouterContext();
    	let teams = [{id:1, code:"team1", name:"team1", _links: {self: {href:"/1"}}}, {id:2, code:"team2", name:"team2", _links: {self: {href:"/2"}}}]
    	let props = {isFetching: false, teams: teams}
        let component = shallow(<TeamList {...props}/>, {context: context});

        expect(component.node.type).toEqual('div');
        expect(component.node.props.children.type).toEqual('ul');
        expect(component.node.props.children.props.children.length).toEqual(2);

        expect(component.node.props.children.props.children[0].type.name).toEqual('Team');
        expect(component.node.props.children.props.children[0].props.code).toEqual('team1');
        expect(component.node.props.children.props.children[0].props.url).toEqual('/playground/teams/1/players');


        expect(component.node.props.children.props.children[1].type.name).toEqual('Team');
        expect(component.node.props.children.props.children[1].props.code).toEqual('team2');
        expect(component.node.props.children.props.children[1].props.url).toEqual('/playground/teams/2/players');
    });

});