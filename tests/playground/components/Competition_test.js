import { renderIntoDocument } from 'react-addons-test-utils';
import React from 'react'
import { findDOMNode } from 'react-dom'
import { shallow } from 'enzyme';
import Competition from '../../../src/playground/components/Competition'
import createRouterContext from 'react-router-test-context'


describe('Competition Component', () => {
    it ('should render the component with caption and url', () => {
    	let context = createRouterContext()
        let component = shallow(<Competition id="1" caption="caption_test" league="test" url="url" numberOfTeams="20"/>, {context: context});
        expect(component.node.type).toEqual('li');
        expect(component.node.props.children.props.children).toEqual('caption_test');
        expect(component.node.props.children.props.to).toEqual('url');
    });
});