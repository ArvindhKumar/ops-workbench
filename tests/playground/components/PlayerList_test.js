import { renderIntoDocument } from 'react-addons-test-utils';
import React from 'react'
import { findDOMNode } from 'react-dom'
import { shallow } from 'enzyme';
import PlayerList from '../../../src/playground/components/PlayerList'
import createRouterContext from 'react-router-test-context'


describe('PlayerList Component', () => {
    it ('should render the loading when isFetching is true', () => {
    	let context = createRouterContext();
    	let props = {isFetching: true, teams: []}
        let component = shallow(<PlayerList {...props}/>, {context: context});
        
        expect(component.node.type).toEqual('div');
        expect(component.node.props.children.props.children).toEqual('Loading Players...');
    });

    it ('should render players when isFetching is false', () => {
    	let context = createRouterContext();
    	let players = [{id:1, name:'player1', jerseyNumber:1}, {id:2, name:'player2',jerseyNumber:2}]
    	let props = {isFetching: false, players: players}
        let component = shallow(<PlayerList {...props}/>, {context: context});

        expect(component.node.type).toEqual('div');
        expect(component.node.props.children.type).toEqual('ul');
        expect(component.node.props.children.props.children.length).toEqual(2);

        expect(component.node.props.children.props.children[0].type.name).toEqual('Player');
        expect(component.node.props.children.props.children[0].props.name).toEqual('player1');
        expect(component.node.props.children.props.children[0].key).toEqual("1");


        expect(component.node.props.children.props.children[1].type.name).toEqual('Player');
        expect(component.node.props.children.props.children[1].props.name).toEqual('player2');
        expect(component.node.props.children.props.children[1].key).toEqual("2");
    });

});