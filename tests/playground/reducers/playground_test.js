import { renderIntoDocument } from 'react-addons-test-utils';
import React from 'react'
import { findDOMNode } from 'react-dom'
import { shallow } from 'enzyme';
import _ from 'lodash'
import playground from '../../../src/playground/reducers/playground'
import createRouterContext from 'react-router-test-context'


describe('playground reducer', () => {
    it ('REQ_COMP it should set isFetching true', () => {
    	let previousState = []
    	let action = {type: 'REQ_COMP'}
    	let nextState = playground(previousState, action)
        expect(nextState.isFetching).toEqual(true);
    });

    it ('REQ_TEAM it should set isFetching true', () => {
    	let previousState = []
    	let action = {type: 'REQ_TEAM'}
    	let nextState = playground(previousState, action)
        expect(nextState.isFetching).toEqual(true);
    });

    it ('REQ_PLAYER it should set isFetching true', () => {
    	let previousState = []
    	let action = {type: 'REQ_PLAYER'}
    	let nextState = playground(previousState, action)
        expect(nextState.isFetching).toEqual(true);
    });

    it ('REC_COMP it should set isFetching false and set competitions', () => {
    	let previousState =  {isFetching: true}
    	let action = {type: 'REC_COMP', competitions: [{id:1}, {id:2}]}
    	let nextState = playground(previousState, action)
        expect(nextState.isFetching).toEqual(false);
        expect(nextState.competitions.length).toEqual(2);
    });

    it ('REC_TEAM it should set isFetching false and set teams', () => {
    	let previousState = {isFetching: true, competitions: [{id:1}, {id:2}]}
    	let action = {type: 'REC_TEAM', teams: {teams: [{id:1}, {id:2}]}, competitionId:"1"}
    	let nextState = playground(previousState, action);
    	let comp = _.find(nextState.competitions, function(comp){
    		return comp.id.toString() == action.competitionId
    	})
        expect(nextState.isFetching).toEqual(false);
        expect(comp.teams.length).toEqual(2);
    });

    it ('REC_PLAYER it should set isFetching false and set competitions', () => {
    	let previousState = {isFetching: true, competitions: [{id:1, teams:[{id:1, _links:{self:{href:"/1"}}}]}, {id:2}]}
    	let action = {type: 'REC_PLAYER', players: {players: [{id:1}, {id:2}]}, teamId: "1"}
    	let nextState = playground(previousState, action)
    	let comp = _.find(nextState.competitions, function(comp){
    		return comp.id.toString() == "1"
    	})
    	let teams = _.find(comp.teams, function(team){
    		return team.id.toString() == action.teamId
    	})
        expect(nextState.isFetching).toEqual(false);
        expect(teams.players.length).toEqual(2);
    });

});