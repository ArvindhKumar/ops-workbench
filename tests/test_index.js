const testsContext = require.context(".", true, /_test$/);

testsContext.keys().forEach(testsContext);

const sourceContext = require.context("../src/playground/components/", true, /js$/);

sourceContext.keys().forEach(sourceContext);
